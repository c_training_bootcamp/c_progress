/* 3 visualizar la tarifa de la luz segun el gasto de correinte electrica.
para un gasto menor de 1.00kwxh la tarifa es 1.2
entre 1.000y 1.850 la tarifa es 1.0
y para gastos mayores a 1.850 la tarifa es de 0.9 */
#include<stdio.h>
#define TARIFA1 1.2
#define TARIFA2 1.0
#define TARIFA3 0.9
int main(void){
	float consumo,tasa;
	printf("Indique su consumo en kw:\n");
	scanf("%f",&consumo);
	if(consumo < 1.000)
	{
		tasa = TARIFA1;
	}
	if(consumo > 1.000 && consumo < 1.850)
	{
		tasa = TARIFA2;
	}
	if(consumo > 1.850)
	{
		tasa = TARIFA3;
	}
	printf("\nSu tasa de consumo es: %.1f\n",tasa);
	return 0;
}
