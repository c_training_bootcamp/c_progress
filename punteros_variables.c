#include<stdio.h>
int main(void)
{
int numero = 2;
float decimal = 2300;
char letra = 'a';

/*definiendo punteros */
int *p_numero;
float *p_decimal;
char *p_letra;
p_numero = &numero;
p_decimal = &decimal;
p_letra = &letra;
/*mostrando por pantalla las posiciones segun su puntero */
printf("Numero:%i \t posicion: %p\n",*p_numero,&p_numero);
printf("Numero:%2.f \t posicion %p\n",*p_decimal,&p_decimal);
printf("Letra: %c \t posicion:%p\n",*p_letra,&p_letra);
return 0;
}
