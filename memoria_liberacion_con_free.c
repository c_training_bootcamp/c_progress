#include<stdio.h>
#include<stdlib.h>
int main(void)
{
int *num;
num = malloc(sizeof(int));
if(num == NULL)
{
printf("No memory left to allocate num");
}
else
{
printf("theres space for num \n");
*num = 10;
printf("%i\n",*num);
free(num);
}
printf("This is the value of num after free is called:%i\n",*num);
return 0;
}
